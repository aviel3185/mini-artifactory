import json
import os
import shutil


def getPackages():
    dir_abs_path = os.path.dirname(__file__)
    packages_rel_path = './packages.json'
    packages_abs_path = os.path.join(dir_abs_path, packages_rel_path)
    packages = open(packages_abs_path, 'r')
    return json.loads(packages.read())

def setPackages(package_list):
    dir_abs_path = os.path.dirname(__file__)
    packages_rel_path = './packages.json'
    packages_abs_path = os.path.join(dir_abs_path, packages_rel_path)
    packages = open(packages_abs_path, 'w')
    packages.write(json.dumps(package_list))

def downloadOutdated():
    if os.path.exists('packages') == False:
        os.mkdir('packages')

    packages = getPackages()

    for attr, value in packages.items():
        ver = os.popen('npm show ' + attr + ' version').read().strip()

        if value == 'true':
            os.system('npo fetch ' + attr +
                      '@latest --no-cache --dest packages')
            packages[attr] = ver

        else:
            current_ver = value.split('.')
            new_ver = ver.split('.')

            if (current_ver[2] < new_ver[2]) or (current_ver[1] < new_ver[1]) or (current_ver[0] < new_ver[0]):
                packages[attr] = ver
                os.system('npo fetch ' + attr +
                          '@latest --no-cache --dest packages')

    # shutil.copyfile('packages.tar', 'F:')
    setPackages(packages)
    shutil.rmtree('packages')


downloadOutdated()
