# Mini Artifactory
A Python script for updating multiple npm packages in a private registry.


## Prerequisites
- Python 3 
- NPM
- npm-offline-packager - "npm i -g npm-offline-packager"

## Usage
1. Edit the packages.json file to contain a JSON formatted file with a list of wanted libraries.
	- "true" value for a package will be downloaded and replaced by the latest version
	- "8.0.2" value (version value) will the checked for a newer version from the public npm registry, if found the newest library will be downloaded and the version will be replaced by the latest.
2. For using the script just execute it (by python).
3. Upload the created "packages.tar" file by "npo publish <path to packages.tar>